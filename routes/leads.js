const express = require("express");
const bodyParser = require('body-parser')

const fs = require('fs');


const router = express.Router();

// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
router.use(bodyParser.json())

router.use((req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  next()
})

const pathfile = './leads.json';

router.post('/', function (req, res) {
  res.writeHead(200);
  const leadStrings = JSON.stringify(req.body, null, 2);
  
  fs.stat(pathfile, (err, stats) => {
    if (err) {
      fs.writeFile(pathfile, `[${leadStrings}]`, { flag: 'wx' }, (err) => {
        if (err) throw err;
      });
    } else {
      fs.readFile(pathfile, 'utf8', (err, data) => {
        if (err) throw err;
        
        const updatedData = `${data.slice(0, -1)},${leadStrings}]`;
        fs.writeFile(pathfile, updatedData, (err) => {
          if (err) throw err;
        });
      });
    }
  });

  res.end(leadStrings);
});

router.get('/', function (req, res) {
  res.writeHead(200);
  const leadStrings = JSON.stringify(req.body, null, 2);
  
  fs.stat(pathfile, (err, stats) => {
    if (err) {
      res.end('Leads = 0');
    } else {
      fs.readFile(pathfile, 'utf8', (err, data) => {
        if (err) throw err;
        res.end(data);
      });
    }
  });
});


module.exports = router;
