const express = require("express");

const router = express.Router();

router.use("/leads", require("./leads"));

module.exports = router;
