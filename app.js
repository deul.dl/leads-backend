const http = require('http');
const express = require("express");

const app = express();


app.use("/", require("./routes"));

const server = http.createServer(app);

server.listen(8080, (error) => {
  if (error) {
    console.error(error.message);
  } else {
    const addr = server.address();
    const bind = typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
    console.log(`Listening on ${bind}`);
  }
});
